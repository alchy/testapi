Page Object Design-Naming Convention
====================

List all element by model on the page by their usage. This will help differentiate between button, link, element.

### All page object must have a Path and all action has Element to it.
 + `this.nameInputPath = by.model('user.username');`
 + `this.privateRadioButtonPath = by.xpath("//*[@type='radio' and @value='private']");`

### Path and Element:
+ Button Syntax: 
 + `this.<name>ButtonPath = by.xpath();`
 + Example: 
   + `this.loginButtonPath = by.xpath('//*[@class="btn btn-default"][contains(text(),"Login")]');`
+ Input Syntax: 
 + `this.<name>InputPath = by.css('[ng-model="xxx"]');`  (If this has a ng-model)
 + Example: 
   + `this.passwordInputPath = by.css('[ng-model="user.password"]');`

### Text
+ Text Syntax: ```this.<name>Text = <Text>```
 + Example: ```this.loginText = 'Login In'```

### Actions - Click an element
+ Syntax: ```this.click<nameofButtonPath>Element = function()```
 + Example: ```this.clickloginButtonElement = function ()```

### Actions - Set an input
+ Syntax: `this.set<nameofInputPath>Element = function ()`
 + Example: `this.setpasswordInputElement = function ()`

### Assertions - Text
+ Syntax: ```this.is<nameofTextPath>TextPresent = function()```
 + Example: ```this.isloginTextPresent = function () ```

*** We won't be using by.model or by.buttonText as this are direct angular call. We have encounter in our app that sometime our pages switch from angular to non angular.  When this occur, we have a fallback method inside commons::findElement to switch.  But this won't work with angular path.  So for our app to work with both, we won't be using these function.