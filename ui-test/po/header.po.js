/**
 * @ngdoc object
 * @name header
 * @description 
 * 	 Global header section of all pages.  <br>
 *
 */

var commons = require('../node_modules/protractor-core/commons/commons.js');
var commonsMethod = new commons();

var header = function () {
  'use strict';
  var that = this;


  /******************************************************
  * Page Object Elements
  *******************************************}***********/
	this.nameFirstLastPath = function (nameFirstLast) {
    return by.xpath("//*[@class='hidden-xs ng-binding'][contains(text(),'" + nameFirstLast + "')]");
  };

  this.logoutLinkPath = by.css("a[ng-click='logout()']");
  this.logoPath = by.id("nav-logo");
  this.usernameDropdownPath = by.css("div[class='user-name dropdown']");
  this.alertCloseIconPath = by.css("[ng-click='close()']");
  this.deskopNotificationLinkPath = by.cssContainingText('a.ng-scope[ng-click="requestNotif()"]','Click Here');
  this.companyNamePath = function(companyName){
    //return by.xpath("//*[contains(@class,'ng-binding')][contains(text(), '" + companyName + "')]");
    return by.repeater("org in user.organizations").column(companyName);
  }

  /******************************************************
  * Page Object Texts
  ******************************************************/

	this.userOrganizationRepeaterText = 'org in user.organizations';
	
  /******************************************************
  * Page Object Methods
  ******************************************************/

  /**
   * @ngdoc Methods
   * @name isNamePresent
   * @methodOf header
   * @description Main login method. Click login button. Wait for rooms to load and verify username is present.
   *
   * @param {string} nameFirstLast name that will be verify on header
   */
  this.isNamePresent = function (nameFirstLast) {
    commonsMethod.findElement(that.nameFirstLastPath(nameFirstLast)).then(function (foundElement) {
      foundElement.getText().then(function(text){
        expect(text).toEqual(nameFirstLast);
        console.log(nameFirstLast + ' has login successfully.');
      });

    });
  };

  /**
   * @ngdoc Methods
   * @name clicknameFirstLastElement
   * @methodOf header
   * @description  click on the user dropdown.
   *
   */

  this.clicknameFirstLastElement = function (nameFirstLast) {
    commonsMethod.findElement(that.nameFirstLastPath(nameFirstLast)).then(function (foundElement) {
      foundElement.click();
    });

  };

  /**
   * @ngdoc Methods
   * @name clicklogoutLinkElement
   * @methodOf header
   * @description Click 'logout' option from dropdown.
   *
   */

  this.clicklogoutLinkElement = function () {
    commonsMethod.findElement(that.logoutLinkPath).then(function (foundElement) {
      foundElement.click();

    });
  };

  /**
   * @ngdoc Methods
   * @name clicklogoElement
   * @methodOf header
   * @description Click on  Elance-odesk logo.
   *
   */

  this.clicklogoElement = function () {
    commonsMethod.findElement(that.logoPath).then(function (foundElement) {
      foundElement.click();

    });
  };

  /**
   * @ngdoc Methods
   * @name clickusernameDropdownElement
   * @methodOf header
   * @description Click on username dropdown
   *
   */

  this.clickusernameDropdownElement = function () {
    commonsMethod.findElement(that.usernameDropdownPath).then(function (foundElement) {
     foundElement.click();
    });
  };

    /**
     * @ngdoc Methods
     * @name clickalertCloseIconElement
     * @methodOf header
     * @description Click alert close icon.
     *
     */

    this.clickalertCloseIconElement = function () {
        commonsMethod.findElement(that.alertCloseIconPath).then(function (foundElement) {
            foundElement.click();
        });
    };

  /**
   * @ngdoc Methods
   * @name clickCompanyNameElement
   * @param {string} companyName Name of company
   * @methodOf header
   * @description Click on  company name in the Companies list.
   */
  this.clickcompanyNameElement = function(companyName){
//      .then(function(){
//      element.all(by.xpath("//ul/li[@ng-repeat='org in user.organizations']/a")).reduce(function(acc, elem){
//        elem.getText().then(function(text){
//          console.log(text);
//          return acc + text + ' ';
//        });
//      });
/*      element.all(by.repeater(that.userOrganizationRepeaterText)).reduce(function(acc, elem){
        elem.getText().then(function(text){
          text = text.trim();
          companyName = companyName.toString().trim();

          if(text==companyName){
            console.log(text + " is selected");
            elem.click();
          }
        });
      });
    element.all
    */
    element.all(by.repeater(that.userOrganizationRepeaterText)).each(function(foundElement){
      foundElement.getWebElement().getText().then(function(text){
        if(text.substring(0,companyName.length)==companyName){
          console.log(text + " is selected");
          foundElement.getWebElement().click();
        }
      });
    });// if this script is not working, use the old script and comment this
  };

  /**
   * @ngdoc Methods
   * @name clickDeskopNotificationLinkElement
   * @methodOf header
   * @description Click desktop notifications settings
   */
  this.clickDesktopNotificationLinkElement = function () {
    commonsMethod.findElement(that.deskopNotificationLinkPath).then(function (foundElement) {
      foundElement.click();
    });
  };

  /**
   * @ngdoc Methods
   * @name verifyDesktopNotificationMessage
   * @methodOf header
   * @description verify the given message is equal to current desktop notification message
   */
  this.verifyDesktopNotificationMessage =function(message){
    var values =element.all(by.xpath("//div[@ng-transclude='']"));
    values.filter(function(elem, index){
      return elem.getText().then(function(value) {
        console.log(value);
        return value.indexOf(message) != -1;
      });
    }).then(function(filterElements){
      expect(filterElements[0].isDisplayed()).toBe(true);
    });
  }

};
module.exports = header;	//naming the page object
