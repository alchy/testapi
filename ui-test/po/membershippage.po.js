/**
 * @ngdoc object
 * @name membershipage
 * @description
 * 	Login page object.  <br>
 *   	PageURL: / <br>
 * 	See {@link https://github.com/angular/angular.js/wiki/Writing-AngularJS-Documentation} for ngdoc block syntax.
 *
 */
var commons = require('../node_modules/protractor-core/commons/commons.js');
var commonsMethod = new commons();

var membershipPage = function () {

  'use strict';
  var that = this;

  beforeEach(function () {

  });

  /******************************************************
   * Page Object Elements (Path and Element)
   ******************************************************/

  /**
   * @ngdoc property
   * @name pageURL
   * @propertyOf loginPage
   * @description URL of the login page
   */
  this.pageUrl = '/monetization/membership/index';	//baseURL will already be known by protractor


  this.editMembershipLinkTextPath = by.xpath('//div[@class="line2"]/*[contains(text(),"View or edit membership plan")]');
  this.addMoreConnectsLinkTextPath = by.xpath('//div[@class="line2"]/*[contains(text(),"Add more Connects")]');
  this.learnMoreLinkTextPath = by.xpath('//*[contains(text(),"Learn More")]');
  //div[@class="line2"]/*[contains(text(),'View or edit membership plan')]

  /******************************************************
   * Page Object Texts
   ******************************************************/


  /******************************************************
   * Page Object Methods
   ******************************************************/
  this.getUrl = function () {
    commonsMethod.getUrl(that.pageUrl);
  };




  this.clickeditMembershipLinkElement = function(){
    commonsMethod.findElement(that.editMembershipLinkTextPath).then(function (foundElement) {
      foundElement.click().then(function() {
        console.log('click on the link')
      });
    });
  };

  this.clickaddMoreConnectsLinkTextElement = function(){
    commonsMethod.findElement(that.addMoreConnectsLinkTextPath).then(function (foundElement) {
      foundElement.click();
    });

  };


  this.learnMoreLinkTextPathElement = function(){
    commonsMethod.findElement(that.learnMoreLinkTextPath).then(function (foundElement) {
      foundElement.click();
    });
  };

  this.getTitulo = function(){
    return element(that.addMoreConnectsLinkTextPath).getText();
  };

  this.isLinkElementPresent = function () {
    commonsMethod.findElement(that.addMoreConnectsLinkTextPath).then(function (foundElement){
      expect(browser.isElementPresent(foundElement));
    });
  };




};



module.exports = membershipPage;	//naming the page object