
/**
 * @ngdoc object
 * @name addConnectsPage
 * @description
 * 	Login page object.  <br>
 *   	PageURL: / <br>
 * 	See {@link https://github.com/angular/angular.js/wiki/Writing-AngularJS-Documentation} for ngdoc block syntax.
 *
 */
var commons = require('../node_modules/protractor-core/commons/commons.js');
var commonsMethod = new commons();

var addConnectsPage = function () {

  'use strict';
  var that = this;

  beforeEach(function () {

  });

  /******************************************************
   * Page Object Elements (Path and Element)
   ******************************************************/

  /**
   * @ngdoc property
   * @name pageURL
   * @propertyOf loginPage
   * @description URL of the login page
   */
  this.pageUrl = '/membership/add-connects';	//baseURL will already be known by protractor


  this.backLinkTextPath = by.css('[data-ng-click="addConnectsBack()"]');
  this.availableConnectsPath = by.css('.column.two.first-row>strong');
  this.addConnectsComboBoxPath = by.css('[data-ng-model = "addConnectsObj.numberOfConnects"]');
  this.addConnectsButtonPath = by.css('[data-ng-click="addConnectsFinish()"]');
  this.alertSuccessConfirmationPath = by.css('.alert-success');
  this.alertDangerErrorPath =  by.css('.alert-danger.ng-binding');


  /******************************************************
   * Page Object Texts
   ******************************************************/

  this.alertSuccessConfirmationText = 'Your Connects balance has been updated!';
  this.alertDangerErrorText = 'An error has occurred while purchasing additional connects.';

  /******************************************************
   * Page Object Methods
   ******************************************************/
  this.getUrl = function () {
    commonsMethod.getUrl(that.pageUrl);
  };


  /**
   * @ngdoc Methods
   * @name clickbackLinkTextElement
   * @methodOf addConnectsPage
   * @description Click on back link text
   *
   */
  this.clickbackLinkTextElement = function () {
    commonsMethod.findElement(that.backLinkTextPath).then(function (foundElement) {
      foundElement.click();
    });
  };


  /**
   * @ngdoc Methods
   * @name clickaddConnectsButtonElement
   * @methodOf addConnectsPage
   * @description Click on addConnectsButton link text
   *
   */
  this.clickaddConnectsButtonElement = function () {
    commonsMethod.findElement(that.addConnectsButtonPath).then(function (foundElement) {
      foundElement.click();
    });
  };


  /**
   * @ngdoc Methods
   * @name clickaddConnectsButtonElement
   * @methodOf addConnectsPage
   * @description Click on addConnects ComboBox.
   *
   */
  this.clickaddConnectsComboBoxElement = function () {
    commonsMethod.findElement(that.addConnectsComboBoxPath).then(function (foundElement) {
      foundElement.click();
    });
  };


  this.getAvailableConnectsElement = function () {
    var  e = element(that.availableConnectsPath);
    e.getText().then(function(text){
      console.log(text);
    });
    return e.getText();

  };

  this.selectConnectsAmount = function (optionNumber) {

    if(optionNumber){

       element(that.addConnectsComboBoxPath).getWebElement().findElements(by.tagName('option'))
        .then(function(options){
          options[optionNumber].click();
        });

    }else{
       element(that.addConnectsComboBoxPath).all(by.tagName('option')).last().click();

    }

  };

  this.isalertSuccessConfirmationPresent = function(){

  }



};
module.exports = addConnectsPage;	//naming the page object