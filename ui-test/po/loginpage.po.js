/**
 * @ngdoc object
 * @name loginPage
 * @description 
 * 	Login page object.  <br>
 *   	PageURL: / <br>
 * 	See {@link https://github.com/angular/angular.js/wiki/Writing-AngularJS-Documentation} for ngdoc block syntax.
 *
 */

var commons = require('../node_modules/protractor-core/commons/commons.js');
var commonsMethod = new commons();
var header = require('./header.po.js');
var globals = require('../node_modules/protractor-core/commons/globals.js');


var loginPage = function () {

  'use strict';
  var that = this;

  beforeEach(function () {

  });

  /******************************************************
   * Page Object Elements (Path and Element)
   ******************************************************/
  
  /**
   * @ngdoc property
   * @name pageURL
   * @propertyOf loginPage
   * @description URL of the login page
   */
  this.pageUrl = '/login?_redirected';	//baseURL will already be known by protractor


  this.usernameInputPath = by.css('#username');
  this.passwordInputPath = by.css('#password');
  this.loginButtonPath = by.css('#submit');
  this.visitoridInputPath = by.xpath('//*[@id="visitor_id"]');
  this.devSessionIdInputPath = by.xpath('//*[@id="dev03_session_id"]');
  this.qtVisitorIdInputPath = by.xpath('//*[@id= "qt_visitor_id"]');
  
  /******************************************************
   * Page Object Texts
   ******************************************************/

  /**
   * @ngdoc property
   * @name signInText
   * @propertyOf loginPage
   * @description 'Sign In' text
   */
  this.loginText = 'Log In';


  /******************************************************
   * Page Object Methods
   ******************************************************/

  /**
   * @ngdoc Methods
   * @name getUrl
   * @methodOf loginPage
   * @description Open up login page, will wait for angularjs page to load, and verify Url.
   *
   */
  this.getUrl = function () {
    commonsMethod.getUrl(that.pageUrl);
  };



  /**
   * @ngdoc Methods
   * @name clickloginButtonElement
   * @methodOf loginPage
   * @description Function to login. It will click login after entering username/password. Don't use this if you want to wait for next page to load.
   *
   * @param {string} name username you wish to enter
   * @param {string} password password you wish to enter
   */

  this.clickloginButtonElement = function(){
    commonsMethod.findElementDriver(that.loginButtonPath).then(function (foundElement) {
      foundElement.click().then(function() {
        console.log('Loggin in ')
      });
    });
  };

  /**
   * @ngdoc Methods
   * @name setvisitoridInputElement
   * @methodOf loginPage
   * @description Send input to the username field
   *
   * @param {string} value username you wish to enter
   */
  this.setvisitoridInputElement = function (value) {
    commonsMethod.findElement(that.visitoridInputPath).then(function (foundElement) {
      foundElement.clear();
      foundElement.sendKeys(value);
    });
  };

  /**
   * @ngdoc Methods
   * @name setdevSessionIdInputElement
   * @methodOf loginPage
   * @description Send input to the username field
   *
   * @param {string} value username you wish to enter
   */
  this.setdevSessionIdInputElement = function (value) {
    commonsMethod.findElement(that.devSessionIdInputPath).then(function (foundElement) {
      foundElement.clear();
      foundElement.sendKeys(value);
    });
  };


  /**
   * @ngdoc Methods
   * @name setqtVisitorIdInputElement
   * @methodOf loginPage
   * @description Send input to the username field
   *
   * @param {string} value username you wish to enter
   */
  this.setqtVisitorIdInputElement = function (value) {
    commonsMethod.findElement(that.qtVisitorIdInputPath).then(function (foundElement) {
      foundElement.clear();
      foundElement.sendKeys(value);
    });
  };

  /**
   * @ngdoc Methods
   * @name setusernameInputElement
   * @methodOf loginPage
   * @description Send input to the username field
   *
   * @param {String} value username you wish to enter
   */
  this.setusernameInputElement = function (value) {
    commonsMethod.findElement(that.usernameInputPath).then(function (foundElement) {
      foundElement.clear();
      foundElement.sendKeys(value);
    });
  };


  /**
   * @ngdoc Methods
   * @name setpasswordInputElement
   * @methodOf loginPage
   * @description Send input to the username field
   *
   * @param {String} value username you wish to enter
   */
  this.setpasswordInputElement = function (value) {
    commonsMethod.findElement(that.passwordInputPath).then(function (foundElement) {
      foundElement.clear();
      foundElement.sendKeys(value);
    });
  };
};
module.exports = loginPage;	//naming the page object
