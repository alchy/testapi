/**
 * @ngdoc object
 * @name changeplanpage
 * @description
 * 	Login page object.  <br>
 *   	PageURL: / <br>
 * 	See {@link https://github.com/angular/angular.js/wiki/Writing-AngularJS-Documentation} for ngdoc block syntax.
 *
 */

var commons = require('../node_modules/protractor-core/commons/commons.js');
var commonsMethod = new commons();
var changePlanPage = function () {

  'use strict';
  var that = this;

  beforeEach(function () {

  });

  /******************************************************
   * Page Object Elements (Path and Element)
   ******************************************************/

  /**
   * @ngdoc property
   * @name pageURL
   * @propertyOf loginPage
   * @description URL of the login page
   */
  this.pageUrl = '/membership/change-plan';	//baseURL will already be known by protractor

  this.freeMembershipButtonPath = by.xpath("[data-ng-click="+'"'+"changePlanSelect('3')"+'"'+"]");
  this.PaidMembershipButtonPath = by.xpath("[data-ng-click="+'"'+"changePlanSelect('4')"+'"'+"]");
  this.continuePlanButtonPath = by.css('[class="btn btn-primary btn-lg"]');
  this.backLinkTextPath = by.css('[data-ng-click="goBack()"]');


  /******************************************************
   * Page Object Texts
   ******************************************************/


  /******************************************************
   * Page Object Methods
   ******************************************************/
  this.getUrl = function () {
    commonsMethod.getUrl(that.pageUrl);
  };


  /**
   * @ngdoc Methods
   * @name clickbackLinkTextElement
   * @methodOf
   * @description Click on back link text
   *
   */
  this.clickbackLinkTextElement = function () {
    commonsMethod.findElement(that.backLinkTextPath).then(function (foundElement) {
      foundElement.click();
    });
  };
};
module.exports = changePlanPage;	//naming the page object