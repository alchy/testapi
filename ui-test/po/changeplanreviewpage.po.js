/**
 * @ngdoc object
 * @name changeplanreviewpage
 * @description
 * 	Login page object.  <br>
 *   	PageURL: / <br>
 * 	See {@link https://github.com/angular/angular.js/wiki/Writing-AngularJS-Documentation} for ngdoc block syntax.
 *
 */

var changePlanReviewPage = function () {

  'use strict';
  var that = this;

  beforeEach(function () {

  });

  /******************************************************
   * Page Object Elements (Path and Element)
   ******************************************************/

  /**
   * @ngdoc property
   * @name pageURL
   * @propertyOf loginPage
   * @description URL of the login page
   */
  this.pageUrl = '/login';	//baseURL will already be known by protractor



  /******************************************************
   * Page Object Texts
   ******************************************************/


  /******************************************************
   * Page Object Methods
   ******************************************************/


};
module.exports = changePlanReviewPage;	//naming the page object