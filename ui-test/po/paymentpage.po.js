/**
 * @ngdoc object
 * @name paymentpage
 * @description
 * 	Login page object.  <br>
 *   	PageURL: / <br>
 * 	See {@link https://github.com/angular/angular.js/wiki/Writing-AngularJS-Documentation} for ngdoc block syntax.
 *
 */