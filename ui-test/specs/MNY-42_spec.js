/*
 * JIRA URL: http://jira.odesk.com/browse/MNY-42
 * ID:MNY-42
 * Description: Workplace app logout case
 */

var membershipPage = require('../po/membershippage.po.js');
var loginPage = require('../po/loginpage.po.js');
var addConnectsPage = require('../po/addconnectspage.po.js');

var  loginpage= new loginPage();
var membershippage = new membershipPage();
var addconnectspage = new addConnectsPage();

describe('Monetization MNY-42', function () {


  it('should open login page and sign in', function () {

    browser.ignoreSynchronization = true;
    loginpage.getUrl();
   // browser.get('monetization/membership/index');

    loginpage.setusernameInputElement("mnyqt007");
    loginpage.setpasswordInputElement("Changeme@1!");
    //loginpage.setqtVisitorIdInputElement("172.27.251.15.1416419506036344");
    //loginpage.setdevSessionIdInputElement("9b11586cde5eb3d33511097ee35f008b");
    //loginpage.setvisitoridInputElement("172.27.251.15.1416405441034975");
    loginpage.clickloginButtonElement();

  });

  it ('redirected to membership and connects page', function () {

    //browser.ignoreSynchronization = false;
    membershippage.getUrl();
    membershippage.clickaddMoreConnectsLinkTextElement();
    browser.sleep(2000);

  });

  it ('redirected to add connects page', function () {


    addconnectspage.selectConnectsAmount(1);
    addconnectspage.clickaddConnectsButtonElement();



    /*var s=addconnectspage.getAvailableConnectsElement();

    s.getText().then(function(text){
     if(text=="16"){
       console.log("exito");
     }else{console.log("fracaso")}
    });
*/



  });





});
